<?php

namespace Drupal\evergreen\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining evergreen content
 */
interface EvergreenContentInterface extends ContentEntityInterface {

}
