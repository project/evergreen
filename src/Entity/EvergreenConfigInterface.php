<?php

namespace Drupal\evergreen\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an evergreen entity.
 */
interface EvergreenConfigInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
